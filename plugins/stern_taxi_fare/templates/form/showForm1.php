<?php
function showForm1($atts) {

			$args = array(
				'post_type' => 'stern_taxi_car_type',
				'nopaging' => true,
				'order'     => 'ASC',
				'orderby' => 'meta_value',
				'meta_key' => '_stern_taxi_car_type_organizedBy'
			);
			$allTypeCars = get_posts( $args );	


			
			$args = array(
				'post_type' => 'stern_listAddress',
				'nopaging' => true,
				'meta_query' => array(
					array(
						'key'     => 'typeListAddress',
						'value'   => 'destination',
						'compare' => '=',
					),				
					array(
						'key'     => 'isActive',
						'value'   => 'true',
						'compare' => '=',
					),
				),				
			);
			$allListAddressesDestination = get_posts( $args );			


			 $backgroundColor=get_option('stern_taxi_fare_bcolor');
			 if($backgroundColor!="")
			 {
				$backgroundColor='background-color:'.stern_taxi_fare_hex2rgba($backgroundColor,get_option('stern_taxi_fare_bTransparency'));
			 }
			global $woocommerce;
			
			$currency_symbol = get_woocommerce_currency_symbol();

			if (isBootstrapSelectEnabale()=="true")
			{
				$class = "selectpicker show-tick";
			} else {
				$class = "form-control";
			}
		
			
			
			
			$class_full_row12 = (get_option('stern_taxi_fare_full_row') != "true") ? "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'" : "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'";
			$class_full_row6 = (get_option('stern_taxi_fare_full_row') != "true") ? "class='col-lg-6 col-md-6 col-sm-12 col-xs-12'" : "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'";
			$class_full_row3 = (get_option('stern_taxi_fare_full_row') != "true") ? "class='col-lg-3 col-md-3 col-sm-12 col-xs-12'" : "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'";
			
						
									
					
			$classMain = (get_option('stern_taxi_fare_form_full_row') != "true") ? "class='col-xs-12 col-sm-6 col-lg-6'" : "class='col-xs-12 col-sm-12 col-lg-12'";
			
			showDemoSettings($backgroundColor);
			?>

			
          
   
			<div class="container">
				<div class="stern-taxi-fare">
					<div class="row">
						<div  <?php echo $classMain; ?> id="main1" style="<?php echo $backgroundColor; ?>;padding-bottom: 5px" >

	
	
				<?php 
				$form_dataset = get_option( 'frontend_dataVinfo' );
				//$data_setfd = json_decode($form_dataset, true); 
				$form_dataComp = get_option( 'frontend_dataComp' );
				//$data_setComp = json_decode($form_dataComp, true); 
					
				?>

		<div id="steps_buttons">
			
			<div id="btn_step1">
			<button type="button" class="btn btn-primary" id="btn_step1">
				<span> Step 1 </span> 			
			</button>
			</div>
			<div id="btn_step2">
			<button type="button" class="btn btn-primary" id="btn_step2">
				<span> Step 2 </span> 			
			</button>
			</div>
		
		</div>
		
		
		<div id="step1">
		<div class="row">
		<div class="col col-lg-6">
		<div class="form-group">
		<label for="size">Vehicle Brand</label>
		<select class="form-control calculate" id="vehicle_brand" name="size">
			<option data-price="0.00" value="" selected>-- Vehicle Brand --</option>			
			<?php 
			//$data_unique_vb = array_unique($data_setfd);
			foreach ( $form_dataset as $data_vbrand ) { ?>
			<option data-price="5.00" value="<? echo $data_vbrand[0];?>"><? echo $data_vbrand[0];?></option>
			<?php } ?>
		</select>
		</div>
		</div>
				
		<div class="col col-lg-6">
		<div class="form-group">
		<label for="size">Medel Year</label>
		<select class="form-control calculate" id="model_year" name="size">
			<option data-price="0.00" value="" selected>-- Model Year --</option>
			<?php 
			foreach ( $form_dataset as $data_myear ) { ?>
			<option data-price="5.00" value="<? echo $data_myear[1];?>"><? echo $data_myear[1];?></option>
			<?php } ?>		
		</select>
		</div>
		</div>

		<div class="col col-lg-6">		
		<div class="form-group">
		<label for="packing">Model No.</label>
		<select class="form-control calculate" id="vehicle_modelno" name="packaging">
			<option data-price="0.00" value="Standard" selected>-- Select --</option>
			<?php 
			//$data_unique_mno = array_unique($data_setfd);
			foreach ( $form_dataset as $data_modelNo ) { ?>
			<option data-price="5.00" value="<? echo $data_modelNo[2];?>"><? echo $data_modelNo[2];?></option>
			<?php } ?>		
			</select>
		</div>
		</div>

		<div class="col col-lg-6">
		<div class="form-group">
		<label for="packing">Vehicle Category</label>
		<select class="form-control calculate" id="vehicle_cat" name="packaging">
			<option data-price="0.00" value="Standard" selected>-- Select --</option>
			<?php 
			//$data_unique_vcat = array_unique($data_setfd);
			foreach ( $form_dataset as $data_vcat ) { ?>
			<option data-price="5.00" value="<? echo $data_vcat[3];?>"><? echo $data_vcat[3];?></option>
			<?php } ?>		
		</select>
		</div>
		</div>						
		
		<div id="ins_submodel" class="col col-lg-6">
		<div class="form-group">
		<label for="packing">Sub Model i.e Exi, VXR etc</label>
		<select class="form-control calculate" id="vehicle_submodel" name="packaging">
			<option data-price="0.00" value="" selected>-- Select --</option>
			<?php 
			//$data_unique_submodel = array_unique($data_setfd);
			foreach ( $form_dataset as $data_SubModel ) { ?>
			<option data-price="5.00" value="<? echo $data_SubModel[4];?>"><? echo $data_SubModel[4];?></option>
			<?php } ?>		
		</select>
		</div>
		</div>						
		
	    </div>
	    </div>
	    
		<div id="step2">
		<div class="row">
						
		<div class="col col-lg-6">
		<div class="form-group">
		<label for="packing">Vehicle Category Step 2</label>
		<select class="form-control calculate" id="vehicle_cat_step2" name="packaging">
			<option data-price="0.00" value="Standard" selected>-- Select --</option>
			<?php foreach ( $form_dataComp as $data_vcatstep2 ) { ?>
			<option data-price="5.00" value="<? echo $data_vcatstep2[2];?>"><? echo $data_vcatstep2[2];?></option>
			<?php } ?>		
		</select>
		</div>
		</div>		
		
		<div class="col col-lg-6">
		<div class="form-group">
		<label for="packing">Repair Condition</label>
		<select class="form-control calculate" id="vehicle_repair" name="packaging">
			<option data-price="0.00" value="" selected>-- Select --</option>
			<?php foreach ( $form_dataComp as $data_vcatstep2 ) { ?>
			<option data-price="5.00" value="<? echo $data_vcatstep2[3];?>"><? echo $data_vcatstep2[3];?></option>
			<?php } ?>
		</select>
		</div>
		</div>
		
		<div class="col col-lg-6 checkboxes_step2">
		<div class="form-group">
		<div id="ins_vehicle_modified">
		  <label><input type="checkbox" class="form-control calculate" id="vehicle_modified" value="">Modified?</label>
		</div>		
		<div id="ins_driving_licence">
		  <label><input type="checkbox" class="form-control calculate" id="driving_licence" value="">Driving Licence</label>
		</div>
		<div id="ins_outside_uae">
		  <label><input type="checkbox" class="form-control calculate" id="outside_uae" value="">Outside UAE</label>
		</div>
		<div id="ins_personalAccident">
		  <label><input type="checkbox" class="form-control calculate" id="personal_accident" value="">Personal Accident</label>
		</div>
		<div id="ins_roadsideassistance">
		  <label><input type="checkbox" class="form-control calculate" id="roadside_assistance" value="">Roadside Assistance</label>
		</div>
		<div id="ins_rentaCar">
		  <label><input type="checkbox" class="form-control calculate" id="ins_rentACar" value="">Rent A Car</label>
		</div>
		</div>
		</div>
		
		</div>		
	    </div>
		
		<div style="@display:none;">
		<h4>PRICE</h4>
		<span id="item-price">3.00</span>		
		<div><strong>Min Val</strong> <span id="item_minval">0.00</span></div>		
		<div><strong>Max Val</strong> <span id="item_maxval">0.00</span></div>
		</div>
		<div id="all_comp_data">
		</div>
		

		<!-- Old Calculater -->
		
					<!-- Jquery  Code -->
<?php  
$json_form_dataset = json_encode($form_dataset); 
$json_form_dataComp = json_encode($form_dataComp); 
?>
<script>

jQuery(document).ready(function() {

		var basePrice = 3.00;
		var width;
		


		
		
		jQuery( "#ins_submodel" ).hide();
		jQuery( "#step2" ).hide();
		jQuery( "#btn_step2" ).click(function() {
		  jQuery( "#step1" ).hide();
		  jQuery( "#step2" ).show();
		});		
		jQuery( "#btn_step1" ).click(function() {
		  jQuery( "#step2" ).hide();
		  jQuery( "#step1" ).show();
		});

		jQuery(".calculate").change(function () {

			//Get selected values
			var vehicle_brand = jQuery('#vehicle_brand option:selected').val();
			var selected_modelYear = jQuery('#model_year option:selected').val();
			var selected_vehModelno = jQuery('#vehicle_modelno option:selected').val();
			var selected_vehCat = jQuery('#vehicle_cat option:selected').val();
			var vehicle_submodel = jQuery('#vehicle_submodel option:selected').val();			
			var selected_vehCat2 = jQuery('#vehicle_cat_step2 option:selected').val();			
			var selected_vehicle_repair = jQuery('#vehicle_repair option:selected').val();			
			var sub_model;
			//console("vehicle_submodelCheck..."+vehicle_submodel);
			

			if (jQuery('#vehicle_submodel').val() != 0) {
			   // has a value selected Submodel
			   var selected_valarray = [vehicle_brand, selected_modelYear, selected_vehModelno, selected_vehCat, vehicle_submodel];
			   sub_model = 'true';
			} 
			else {
			   // does not have a value selected Submodel
			   var selected_valarray = [vehicle_brand, selected_modelYear, selected_vehModelno, selected_vehCat];
			   sub_model = 'false';
			}
			
			
			
			//Step1 and selected Data arrays
			var step_1data = <?php echo $json_form_dataset; ?>;
			var step_dataComp = <?php echo $json_form_dataComp; ?>;
			var rt_min_val = 0;
 			jQuery.each( step_1data, function( i, value ) {
				value1 = value[0];
				value2 = value[1];
				value3 = value[2];
				value4 = value[3];
				value5 = value[4];
				selected_valarray1 = selected_valarray[0];
				selected_valarray2 = selected_valarray[1];
				selected_valarray3 = selected_valarray[2];
				selected_valarray4 = selected_valarray[3];
				selected_valarray5 = selected_valarray[4];
				
				//Selected Options array
				if(sub_model == 'true'){	
				rt_selected_valarray = selected_valarray1+selected_valarray2+selected_valarray3+selected_valarray4+selected_valarray5;
				}else{
				rt_selected_valarray = selected_valarray1+selected_valarray2+selected_valarray3+selected_valarray4;	
				}
				
				//Checking if Submodel is selected Fetching Backend Step1 Data
				if(sub_model == 'true'){	
					rt_value = value1+value2+value3+value4+value5;
				}else{
					rt_value = value1+value2+value3+value4;
				}
				
				if(rt_selected_valarray == rt_value){
					rt_min_val = value[5];
					rt_max_val = value[6];
				}		  
			}); 
			var all_companies = [];
			jQuery.each( step_dataComp, function( j, valuecomp ) {
				
				var min_valcomp = valuecomp[4];
				var max_valcomp = valuecomp[5];
				
				if(min_valcomp){
					min_valcomp1 = parseFloat(min_valcomp.replace(/\,/g,''));
				}
				if(min_valcomp){
					max_valcomp1 = parseFloat(max_valcomp.replace(/\,/g,''));
				}
				
				rt_min_val1 = parseFloat(rt_min_val);
				console.log(rt_min_val1);
					if( rt_min_val1>=min_valcomp1&&rt_min_val1<=max_valcomp1){
						if(valuecomp[2] == selected_vehCat2){
							if(valuecomp[3] == selected_vehicle_repair){
								valuecomp_min_val1 = parseFloat(valuecomp[4]);
								valuecomp_rate = parseFloat(valuecomp[6]);
								cal_rate = valuecomp_min_val1 * valuecomp_rate;
								if(cal_rate < valuecomp[7]){
									cal_rate = valuecomp[7]
								}
								var len = valuecomp[6].length;
								all_companies.push({
									compname: valuecomp[0],
									gcc_spec: valuecomp[1],
									vehicle_cat: valuecomp[2],
									repair_cond: valuecomp[3],
									minimum_val: valuecomp[4],
									maximum_val: valuecomp[5],
									rate: valuecomp[6],
									minimum: valuecomp[7],
									dl_less_than_year: valuecomp[8],
									less_than_year: valuecomp[9],
									os_coverage: valuecomp[10],
									pa_driver: valuecomp[11],
									papp: valuecomp[12],
									rs_assistance: valuecomp[13],
									rent_car: valuecomp[14],
									excess_amount: valuecomp[15]
								});
							}
						}
					} 	  
				}); 
			console.log(all_companies);
			if(all_companies){
				jQuery.each( all_companies, function(b, companies) {
					jQuery('#all_comp_data').append('<br>');
					jQuery.each( companies, function( a, compan ) {
					jQuery('#all_comp_data').append(compan);
					});
				});
			}
			var conditional_inc;
		    var min_val;
		    var max_val;
			

			//Getting Jquery Data
			
			
			
			//Vehicle Brand Condition
			if(vehicle_brand == 'toyota'){								
				conditional_inc = 100.0;
			}
			else if(vehicle_brand == 'honda'){								
				conditional_inc = 200.0;
			}
			else if(vehicle_brand == 'audi'){								
				conditional_inc = 300.0;
			}
			
			//Show and hide Sub Model Select Box
			if(selected_modelYear == "2017"){
				//var ins_submodel = document.getElementById("ins_submodel");
				jQuery( "#ins_submodel" ).addClass( " ins_submodel_view" );
				jQuery( "#ins_submodel" ).show();
			}else{
				jQuery( "#ins_submodel" ).removeClass( " ins_submodel_view" );
				jQuery( "#ins_submodel" ).hide();
			}
			
			//if(selected_model == "2009" && selected_vehCat == "saloon"){
			//if(selected_model = 2008 && (vehicle_brand == "Toyota" && Corolla == "Corolla")){
			if((vehicle_brand == "toyota" && selected_modelYear == "2008") || (selected_vehModelno == "Corolla" && selected_vehCat == "Saloon")){
				min_val = 15000;
				max_val = 18000;
			}
			else if((vehicle_brand == "toyota" && selected_modelYear == "2009") || (selected_vehModelno == "Corolla" && selected_vehCat == "Saloon")){
				min_val = 18000;
				max_val = 20000;
			}
			else if((vehicle_brand == "toyota" && selected_modelYear == "2010") || (selected_vehModelno == "Corolla" && selected_vehCat == "Saloon")){
				min_val = 22000;
				max_val = 25000;
			}
			
			//if(document.getElementById('vehicle_modified').checked) {
			if(jQuery("#vehicle_modified").is(':checked')) {
				conditional_inc = conditional_inc + 20;
				//console("vehicle_modified.."+conditional_inc);
			}
			if(jQuery("#driving_licence").is(':checked')) {
				conditional_inc = conditional_inc + 30;
				//console("driving_licence.."+conditional_inc);
			}
			if(jQuery("#outside_uae").is(':checked')) {
				conditional_inc = conditional_inc + 40;
				//console("outside_uae.."+conditional_inc);
			}			
			if(jQuery("#personal_accident").is(':checked')) {
				conditional_inc = conditional_inc + 50;
				//console("personal_accident.."+conditional_inc);
			}			
			if(jQuery("#roadside_assistance").is(':checked')) {
				conditional_inc = conditional_inc + 60;
				//console("roadside_assistance.."+conditional_inc);
			}			
			if(jQuery("#ins_rentACar").is(':checked')) {
				conditional_inc = conditional_inc + 70;
				//console("ins_rentACar.."+conditional_inc);
			}
			
			////console("selected_model.."+conditional_inc);
			////console("selected_vehCat.."+selected_vehCat);
			
			
			//var conditional_inc =  parseFloat(width) * parseFloat(height);
						
			//Apply Conditional Increment
			newPrice = basePrice + parseFloat(conditional_inc);
			//var newPrice = basePrice;

			jQuery(".calculate option:selected").each(function () {

				newPrice += parseFloat(jQuery(this).data('price'));

				////console(typeof newPrice);
				
				//Detect Current Selected Option
				var selected_option = jQuery(this).data('price');
				//console("selected_option..."+selected_option);
				

			});

			newPrice = newPrice.toFixed(2);
			
			//console("newPrice.."+newPrice);
			jQuery("#item-price").html(newPrice);
			jQuery("#item_minval").html(min_val);
			jQuery("#item_maxval").html(max_val);

			

			showEstimatedFareHtml(newPrice);

			localStorage.setItem("finalPrice", newPrice);

			//localStorage.setItem("width", width);
			//localStorage.setItem("height", height);

			});
		});
</script>			

		
		
		
		
		
		
		
		
		
		<!-- Old Calculater -->
		
					
							<form  id="stern_taxi_fare_div" method="post">
								<div style="display:none;" class="row">
									<?php if(get_option('stern_taxi_fare_title') !="") : ?>
										<div id="titleDiv" <?php echo $class_full_row12; ?> style="padding-top: 15px;">
											<h1><?php echo stripslashes(get_option('stern_taxi_fare_title')); ?></h1>										
										</div>
									<?php endif;?>
									
									<?php if(get_option('stern_taxi_fare_subtitle') !="") : ?>
										<div id="subtitleDiv" <?php echo $class_full_row12; ?> style="padding-top: 15px;">
											<h2><?php echo stripslashes(get_option('stern_taxi_fare_subtitle')); ?></h2>										
										</div>
									<?php endif;?>
										
										
										<?php showInputAddress($class_full_row12,"source",$class); ?>
										<?php showInputAddress($class_full_row12,"destination",$class); ?>	

									
									
										<?php if(get_option('stern_taxi_fare_typeCar_calendar_as_input') =="true") : ?>
											<?php showDateTimeInput($class_full_row12,"oneTrip",$class); ?>										
										<?php else: ?>
											<?php showTypeCar($class_full_row6,$class); ?>
											<?php showRoundTrip($class_full_row3,$class); ?>
										<?php endif;?>
										
										
										<?php if(get_option('stern_taxi_fare_seat_field_as_input') =="true") : ?>										
										<?php endif; ?>		
										
										<?php showSeatInputs1($class_full_row3,$class); ?>
										
										<?php if(get_option('stern_taxi_fare_typeCar_calendar_as_input') !="true") : ?>
											<?php if(get_option('stern_taxi_fare_seat_field_as_input') !="true") : ?>
												<?php if(get_option('stern_taxi_fare_use_babySeat') =="true") : ?>
													<?php showBabySeatInputs($class_full_row3,$class); ?>
												<?php endif;?>
											<?php endif;?>
										<?php endif;?>													
																							
									 
									
	
								</div>	
								<?php showButtons(); ?>

								
								
								<input type="hidden"  name="stern_taxi_fare_use_FullCalendar" id="stern_taxi_fare_use_FullCalendar" value="<?php echo get_option('stern_taxi_fare_use_FullCalendar'); ?>"/>
								<input type="hidden"  name="actualVersionPlugin" id="actualVersionPlugin" readonly value="<?php echo getPluginVersion(); ?>"/>
								<input type="hidden"  name="stern_taxi_fare_url_gif_loader" id="stern_taxi_fare_url_gif_loader" value="<?php echo getUrlGifLoader(); ?>"/>									
								<input type="hidden"  name="stern_taxi_fare_show_map" id="stern_taxi_fare_show_map" value="<?php echo get_option('stern_taxi_fare_show_map'); ?>"/>
								<input type="hidden"  name="stern_taxi_fare_auto_open_map" id="stern_taxi_fare_auto_open_map" value="<?php echo get_option('stern_taxi_fare_auto_open_map'); ?>"/>									
								<input type="hidden"  name="getKmOrMiles" id="getKmOrMiles" value="<?php echo getKmOrMiles(); ?>"/>									
								<input type="hidden"  name="stern_taxi_fare_avoid_highways_in_calculation" id="stern_taxi_fare_avoid_highways_in_calculation" value="<?php echo get_option('stern_taxi_fare_avoid_highways_in_calculation'); ?>"/>									
								<input type="hidden"  name="getShow_use_img_gif_loader" id="getShow_use_img_gif_loader" value="<?php echo getShow_use_img_gif_loader(); ?>"/>									
								<input type="hidden"  name="stern_taxi_fare_address_saved_point" id="stern_taxi_fare_address_saved_point" value="<?php echo get_option('stern_taxi_fare_address_saved_point') ?>"/>
								<input type="hidden"  name="stern_taxi_fare_address_saved_point2" id="stern_taxi_fare_address_saved_point2" value="<?php echo get_option('stern_taxi_fare_address_saved_point2') ?>"/>									
								<input type="hidden"  name="First_date_available_in_hours" id="First_date_available_in_hours" value="<?php echo getFirst_date_available_in_hours(); ?>"/>
								<input type="hidden"  name="First_date_available_roundtrip_in_hours" id="First_date_available_roundtrip_in_hours" value="<?php echo getFirst_date_available_roundtrip_in_hours(); ?>"/>
								<input type="hidden"  name="stern_taxi_fare_Time_To_add_after_a_ride" id="stern_taxi_fare_Time_To_add_after_a_ride" value="<?php echo get_option('stern_taxi_fare_Time_To_add_after_a_ride') ?>"/>									
								<input type="hidden"  name="stern_taxi_fare_great_text" id="stern_taxi_fare_great_text" value="<?php _e('Great! ', 'stern_taxi_fare'); ?>"/>
								<input type="hidden"  name="stern_taxi_fare_fixed_price_text" id="stern_taxi_fare_fixed_price_text" value="<?php _e('This is a fixed price ! ', 'stern_taxi_fare'); ?>"/>
								<input type="hidden"  name="stern_taxi_fare_duration" id="stern_taxi_fare_duration" value=""/>
								<input type="hidden"  name="stern_taxi_fare_distance" id="stern_taxi_fare_distance" value=""/>
								<input type="hidden"  name="stern_taxi_fare_nb_toll" id="stern_taxi_fare_nb_toll" value=""/>
								<input type="hidden"  name="stern_taxi_fare_estimated_fare" id="stern_taxi_fare_estimated_fare" value=""/>
								<input type="hidden"  name="stern_taxi_fare_time_wrong" id="stern_taxi_fare_time_wrong" value="<?php _e('Please select a date greater than ', 'stern_taxi_fare'); ?>"/>
								
								
								<div id="divAlertError" class="alert alert-danger alert-dismissible" role="alert" style="display: none;">									
									<div id="divAlertErrorTextStrong">
										<strong><?php _e('Error!', 'stern_taxi_fare'); ?></strong>
										<span id="divAlertErrorText">
										</span>
									</div>
								</div>											
								
								<div id="divAlertSuccess" class="alert alert-success alert-dismissible" role="alert" style="display: none;">									
									<div id="divAlertSuccessTextStrong">
										<strong><?php _e('Great!', 'stern_taxi_fare'); ?></strong>
										<span id="divAlertSuccessText">
										</span>
									</div>
								</div>	
								
														
								
						
							

								<div class="row">
									<div id="resultLeft" style="display: none;">
										<?php if(get_option('stern_taxi_fare_typeCar_calendar_as_input') =="true") : ?>

											
											<?php showRoundTrip($class_full_row3,$class); ?>
											<?php showDateTimeInput($class_full_row12,"roundTrip",$class); ?>
											<?php showTypeCar($class_full_row6,$class); ?>
										
										<?php else : ?>	
											<?php showDateTimeInput($class_full_row12,"oneTrip",$class); ?>
											<?php showDateTimeInput($class_full_row12,"roundTrip",$class); ?>
										<?php endif; ?>
										
										
									</div>
								</div>
								<div class="row">									
									<?php showResults($classMain,$backgroundColor); ?>									
								</div>								
								<div class="row">
									<?php showButtonCheckOut(); ?>
								</div>										
							</form>
						</div>
						
						<div <?php echo $classMain; ?>  id="fullCalendarDivContainer" style="<?php echo $backgroundColor; ?>;display: none;padding-top: 10px;padding-bottom: 10px;">
							<a class='boxclose' id='boxcloseCalendar' onclick='closeBoxCalendar();'></a>
							<div id='external-events'></div>
							<div id='fullCalendarDiv'></div>
						</div>
					
						<div <?php echo $classMain; ?>  id="main2" style="<?php echo $backgroundColor; ?>; background-image:url(<?php echo getUrlGifLoader(); ?>);display: none;">
							<a class='boxclose' id='boxclose' onclick='closeBox();'></a>
							<div id="googleMap" style="width:100%;height:450px;"></div>
						</div>						
				
					</div>
				</div>
				


			</div>
		<?php
	/*
echo "<pre>";
    var_dump(WC()->session);
echo "</pre>";
	*/
	
}
